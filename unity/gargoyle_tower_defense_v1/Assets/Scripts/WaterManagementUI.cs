using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
public class WaterManagementUI : MonoBehaviour
{
	public Text waterpoolText;

	// Update is called once per frame
	void Update () {
		waterpoolText.text = Convert.ToInt32(Waterpool.WaterCount).ToString();
	}
}
