using UnityEngine;
using System.Collections;
using PathCreation;

[RequireComponent(typeof(Enemy))]
public class EnemyMovement : MonoBehaviour {

	public PathCreator pathCreator;
	float distanceTravelled;

	void Update()
	{
		distanceTravelled += GetComponent<Enemy>().speed * Time.deltaTime;
		if (distanceTravelled >= pathCreator.path.length)
			Kill();
		else
		{
			transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
			transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);
			transform.Rotate(0, 0, 90);
		}
	}

	void Kill()
	{
		PlayerStats.Lives--;
		WaveSpawner.EnemiesAlive--;
		Destroy(gameObject);
	}

}
