using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waterpool : MonoBehaviour
{
    public static float WaterCount;
    public static float startWaterCount = 175;

    void Start()
    {
        WaterCount = startWaterCount;
    }
    public static float GetWater(float requieredWater)
    { 
        if (WaterCount - requieredWater > 0)
        {
            WaterCount -= requieredWater;
            return requieredWater;
        }
        else return 0;
        /*{
			var water = WaterCount;
			WaterCount = 0;
            return water;
        }*/
    }
}
