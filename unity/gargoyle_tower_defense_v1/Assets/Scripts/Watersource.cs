using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class Watersource : MonoBehaviour
{
    public List<GameObject> nodeList = new List<GameObject>();
    private List<Color> materialColor = new List<Color>();

    void Start()
    {
        Vector3 waterpoolTransform = gameObject.transform.position;
        foreach (GameObject node in nodeList)
        {
            var nodeScript = node.GetComponent<Node>();
            var distance = Vector3.Distance(waterpoolTransform, node.transform.position);
            nodeScript.distanceFromWatersource = distance;
            nodeScript.watersource = this;
            float H,
                S,
                V;
            Color.RGBToHSV(new Color(0.0f, 0.0f, 1.0f, 1.0f), out H, out S, out V);
            S = (1.0f - Convert.ToSingle(nodeScript.distanceFromWatersource  / 60 )) ;
            if(S > 1.0f)
                S = 1.0f;
            if(S < 0)
                S = 0;
            nodeScript.colorNode = Color.HSVToRGB(H, S, V);
        }
        foreach (GameObject node in nodeList)
        {
            var renderer = node.GetComponent<Renderer>();
            materialColor.Add(renderer.material.color);
        }
    }
    
    void OnMouseEnter()
    {
        FindObjectOfType<AudioManager>().Play("water_01");
        foreach (GameObject node in nodeList)
        {
            var renderer = node.GetComponent<Renderer>();
            var nodeScript = node.GetComponent<Node>();
            renderer.material.color = nodeScript.colorNode;
        }
    }

    void OnMouseExit()
    {
        for (int i = 0; i < materialColor.Count; i++)
        {
            var renderer = nodeList[i].GetComponent<Renderer>();
            renderer.material.color = materialColor[i];
        }
    }
}
