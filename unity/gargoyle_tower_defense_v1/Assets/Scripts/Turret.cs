using System.Collections;
using UnityEngine;
using System;

public class Turret : MonoBehaviour
{
    private Transform target;

    private Enemy targetEnemy;

    [Header("General")]
    public float range = 15f;

    [Header("Use Bullets (default)")]
    public WaterJet projectilePrefab;
    private WaterJet currentProjectile;

    public float fireRate = 1f;

    private float fireCooldown = 0f;

    public Watersource watersource;
    public float distanceFromWatersource;

    public float waterCapacity = 1;

    [Header("Unity Setup Fields")]
    public string enemyTag = "Enemy";

    public Transform partToRotate;

    public float turnSpeed = 10f;

    public float damage = 10f;

    public Transform firePoint;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;
        LockOnTarget();
        if (watersource != null)
        {
            if (fireCooldown <= 0f && (currentProjectile == null || !currentProjectile.hold))
            {
                Shoot();
                fireCooldown = 1f / fireRate;
            }

            fireCooldown -= Time.deltaTime;
        }
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion
            .Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed)
            .eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Shoot()
    {
        WaterJet projectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity);
        projectile.transform.parent = firePoint;
        if (projectile != null && waterCapacity >= 0)
        {
            float water = waterCapacity / (distanceFromWatersource / 20f);
            water = Waterpool.GetWater(water);
            projectile.damage = damage * water;
            if(water != 0) 
            {
                FindObjectOfType<AudioManager>().Play("water_02");
                projectile.Shoot(target, range);
            }
        }
        currentProjectile = projectile;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
