using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterJetController : MonoBehaviour
{
    [SerializeField] bool update;
    [SerializeField] WaterJet jetPrefab;
    [SerializeField] WaterJet projectilePrefab;
    [SerializeField] GameObject targetPrefab;
    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!update) return;
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
                Shoot(hit.point, jetPrefab);
        }
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
                Shoot(hit.point, projectilePrefab);
        }
        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
                target.transform.position = hit.point;
        }
        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
        {
            Destroy(target);
        }
    }

    public void Shoot(Vector3 position, WaterJet prefab)
    {
        target = Instantiate(targetPrefab, position, Quaternion.identity);
        WaterJet jet = Instantiate(prefab, transform.position, Quaternion.identity);
        jet.Shoot(target.transform, 20f);
    }
}
