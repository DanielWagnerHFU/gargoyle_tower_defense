using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioLoader : MonoBehaviour
{  
    public static AudioLoader instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else 
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch(scene.name)
        {
        case "MainMenu":
            FindObjectOfType<AudioManager>().Play("BWV_582_opening");
            break;
        case "LevelSelect":
            FindObjectOfType<AudioManager>().Play("BWV_542_continuation");
            break;
        case "01_Level":
            FindObjectOfType<AudioManager>().Play("BWV_542_opening");
            break;
        }
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}