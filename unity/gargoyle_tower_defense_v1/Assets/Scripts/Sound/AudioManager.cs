using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public Sound[] sounds;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else 
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("sound not found");
            return;
        }
        s.on = true;
        s.source.Play();
    }

    public void StopPlaying (string sound, float fadeoutTime = 0)
    {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null)
            return;
        if (s.source == null)
            return;
        if (s.on == false)
            return;
        if (fadeoutTime == 0) 
        {
            s.source.Stop();
            s.on = false;
        }
        else
        {
            s.on = false;
            StartCoroutine(FadeAudioSource.StartFade(s.source, fadeoutTime, 0));
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch(scene.name)
        {
        case "MainMenu":
            this.StopPlaying("BWV_542_continuation", 0.5f);
            this.StopPlaying("BWV_542_opening", 0.5f);
            this.Play("BWV_582_opening");
            break;
        case "LevelSelect":
            this.StopPlaying("BWV_582_opening", 0.5f);
            this.StopPlaying("BWV_542_opening", 0.5f);
            this.Play("BWV_542_continuation");
            break;
        case "01_Level":
            this.StopPlaying("BWV_582_opening", 0.5f);
            this.StopPlaying("BWV_542_continuation", 0.5f);
            this.Play("BWV_542_opening");
            break;
        }
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
