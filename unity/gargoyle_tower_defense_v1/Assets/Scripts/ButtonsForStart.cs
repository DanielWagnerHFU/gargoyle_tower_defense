using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsForStart : MonoBehaviour
{
	public Text buttonText;
    public Image bgImage;
    void Start()
    {
        bgImage.gameObject.SetActive(false);
        buttonText.gameObject.SetActive(false);
    }
	public void SetTextField () 
    {
        if(bgImage.gameObject.activeSelf == true)
        {
        bgImage.gameObject.SetActive(false);
        buttonText.gameObject.SetActive(false);
        }
        else 
        {
            bgImage.gameObject.SetActive(true);
            buttonText.gameObject.SetActive(true);
        }
	}
}
