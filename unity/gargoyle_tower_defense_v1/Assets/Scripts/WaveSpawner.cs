using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PathCreation;

public class WaveSpawner : MonoBehaviour {

	public static int EnemiesAlive = 0;

	public Wave[] waves;

	public Transform spawnPoint;

	public float timeBetweenWaves = 5f;
	private float countdown = 2f;

	public Text waveCountdownText;

	public GameManager gameManager;

	public PathCreator pathCreator;

	private int waveIndex = 0;

	void Update ()
	{
		if (EnemiesAlive > 0 && waveIndex == waves.Length)
		{
			return;
		}

		if (waveIndex == waves.Length)
		{
			gameManager.WinLevel();
			this.enabled = false;
		}

		if (countdown <= 0f)
		{
			StartCoroutine(SpawnWave());
			countdown = timeBetweenWaves;
			return;
		}

		countdown -= Time.deltaTime;

		countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

		waveCountdownText.text = string.Format("{0:00.00}", countdown);
	}

	IEnumerator SpawnWave ()
	{
		PlayerStats.Rounds++;

		Wave wave = waves[waveIndex];

		EnemiesAlive += wave.count;

		for (int i = 0; i < wave.count; i++)
		{
			SpawnEnemy(wave.enemy);
			yield return new WaitForSeconds(1f / wave.rate);
		}

		Waterpool.WaterCount = Waterpool.startWaterCount;
		waveIndex++;
	}

	void SpawnEnemy (GameObject enemy)
	{
		enemy = Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
		var movement = enemy.GetComponent<EnemyMovement>();
		movement.pathCreator = pathCreator;
		Debug.Log(pathCreator.name);
	}

}
