using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SplineMesh;

public class WaterJet : MonoBehaviour
{
    [SerializeField] Spline spline;
    [SerializeField] ExampleContortAlong contortAlong;
    [SerializeField] Vector3 scale;
    [SerializeField] float peakFactor;
    [SerializeField] float endFactor;
    [SerializeField] float speedDelta;
    [SerializeField] float animationSpeed;
    [SerializeField] ParticleSystem splashParticle;
    [SerializeField] float splashActivationOffset;
    [SerializeField] public bool hold;
    [SerializeField] float slow;
    [SerializeField] float splashRange;

    public float damage;

    private Transform targetTransform;
    private Vector3 target;
    private float range;

    public void Shoot(Transform target, float range)
    {
        this.targetTransform = target;
        this.range = range;
        StopAllCoroutines();
        StartCoroutine(CoroutineShoot());
    }

    IEnumerator CoroutineShoot()
    {
        spline.gameObject.SetActive(false);
        splashParticle.gameObject.SetActive(false);
        target = transform.InverseTransformPoint(targetTransform.position);
        float distance = target.magnitude;
        ConfigureSpline(distance);
        contortAlong.Init();
        spline.gameObject.SetActive(true);

        float meshLength = contortAlong.MeshBender.Source.Length;
        meshLength = meshLength == 0 ? 1 : meshLength;
        float totalLength = meshLength + spline.Length;

        float speedCurveLerp = 0;
        float progress = 0;

        Vector3 startScale = scale;
        startScale.x = 0;
        Vector3 targetScale = scale;

        while (progress < 1)
        {
            distance = target.magnitude;
            if (distance > range)
            {
                distance = range;
                targetTransform = null;
            }
            float end = distance / endFactor - 0.01f;
            if (hold && targetTransform != null || progress < end)
            {
                if (progress < meshLength)
                    // Scale water jet
                    contortAlong.ScaleMesh(Vector3.Lerp(startScale, targetScale, Mathf.InverseLerp(0, end * 0.5f + 0.25f, progress)));

                if (targetTransform != null)
                {
                    // Update target position
                    target = transform.InverseTransformPoint(targetTransform.position);
                    ConfigureSpline(distance);
                    totalLength = meshLength + spline.Length;
                }

                // Update water jet
                contortAlong.Contort(progress);

                // Update progress
                progress += Time.deltaTime * animationSpeed * speedCurveLerp;// / distance;
                if (progress > end) progress = end;
                else speedCurveLerp += speedDelta * Time.deltaTime;

                if (progress == end)
                {
                    // Start splash effect and update its position
                    if (splashParticle.isPlaying == false) {
                        splashParticle.gameObject.SetActive(true);
                        splashParticle.Play();
                        if (targetTransform != null) 
                        {
                            targetTransform.GetComponent<Enemy>().TakeDamage(damage);
                        }
                        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
                        foreach (GameObject enemy in enemies)
                        {
                            if (enemy.transform == targetTransform) continue;
                            float distanceToEnemy = Vector3.Distance(transform.TransformVector(target), enemy.transform.position);
                            if (distanceToEnemy < splashRange) enemy.GetComponent<Enemy>().TakeDamage(damage);
                        }
                    }
                    else if (targetTransform != null && hold)
                    {
                        Enemy enemy = targetTransform.GetComponent<Enemy>();
                        enemy.Slow(slow);
                        enemy.TakeDamage(damage);
                    }
                    splashParticle.transform.localPosition = target;
                    Debug.Log(target);
                }
            }
            else
            {
                contortAlong.Contort(progress);
                progress += 2 * Time.deltaTime * animationSpeed * speedCurveLerp / distance;
                speedCurveLerp += speedDelta * Time.deltaTime;
                if (progress > 0.7f) splashParticle.Stop();
                Debug.Log("expiring");
            }
            yield return null;
        }
        spline.gameObject.SetActive(false);
        Destroy(gameObject, 1f);
    }

    private void ConfigureSpline(float distance)
    {
        Vector3 difference = target.normalized * distance;
        Vector3 direction = difference / 3f;
        Vector3 peak = Vector3.up * distance * peakFactor;
        spline.nodes[0].Position = Vector3.zero;
        spline.nodes[0].Direction = peak + direction;
        spline.nodes[1].Position = target;
        spline.nodes[1].Direction = target - peak + direction;
    }
}
