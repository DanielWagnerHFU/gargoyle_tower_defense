using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Wavespwaner : MonoBehaviour
{
    public Transform enemyPreFab;

    public Transform spawnPoint;
    public float timeBetweenWaves = 5f;
    private float countdown = 2f;

    public Text WaveCountdownText;
    private int waveIndex = 0;


    void Update()
    {
        if(countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
        }
        countdown -= Time.deltaTime;
        WaveCountdownText.text = Mathf.Floor(countdown).ToString();
    }
    IEnumerator SpawnWave()
    {
        waveIndex++;
        for(int i = 0; i < waveIndex; i++)
        {
            SpwanEnemy();
            yield return new WaitForSeconds(0.5f);
        }
    }
    void SpwanEnemy()
    {
        Instantiate(enemyPreFab, spawnPoint.position, spawnPoint.rotation);
    }

}
